# Start Typing

Simulates typing of text.


## Usage

Use the following HTML tag properties:

- `data-starttyping` (string) - the text to be typed
- `data-typeinsec` (integer)  - number of seconds, within which the animation should complete
- `data-typingdone` (string)  - callback function, gets executed once typing is complete


#### Example
```
<div
    data-starttyping="My Awesome Text To Animate"
    data-typeinsec="3"
    data-typingdone="my_awesome_callback_function" ...></div>
```
