/**
 * Start Typing
 * ------------
 * Simulates typing of text.
 * 
 * Usage (via HTML tag properties):
 *     'data-starttyping' (string) - the text to be typed
 *     'data-typeinsec' (integer)  - number of seconds, within which the animation should complete
 *     'data-typingdone' (string)  - callback function, gets executed once typing is complete
 *
 **/

jQuery(document).ready(function() {
	
	jQuery('[data-starttyping]').each(function(i, starttyping) {
		st_starttyping(jQuery(starttyping));
	});
	
});

function st_starttyping($element) {
	var text_to_type = $element.data('starttyping'),
		text_arr = text_to_type.split(''),
		type_in_sec = $element.data('typeinsec'),
		typing_done = $element.data('typingdone'),
		type_delay = (typeof(type_in_sec) != 'undefined' && !isNaN(type_in_sec)) ? Math.ceil((type_in_sec*1000)/text_arr.length) : 400;
	$element
		.data('st_text', text_to_type)
		.data('st_delay', type_delay)
		.data('st_callback', typing_done)
		.removeAttr('data-starttyping data-typeinsec data-typingdone')
		.removeData('starttyping typeinsec typingdone');
	$element.data('st_interval', setInterval(function() { st_type($element); }, type_delay));
}
function st_type($element) {
	var current_index = typeof($element.data('st_index')) != 'undefined' ? $element.data('st_index') : -1,
		text_to_type = $element.data('st_text');
	current_index++;
	if(text_to_type.length >= current_index) {
		$element.html(text_to_type.substring(0, current_index));
		$element.data('st_index', current_index);
	} else {
		clearInterval($element.data('st_interval'));
		$element.addClass('typed animate');
		if(typeof($element.data('st_callback')) != 'undefined'
		&& typeof(window[$element.data('st_callback')]) == 'function')
			window[$element.data('st_callback')]();
	}
}